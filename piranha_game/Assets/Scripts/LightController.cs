﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour
{
    [SerializeField]
    Light _light;

    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            _light.range = _light.range - 1;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            _light.range = _light.range + 1;
        }
    }
}
