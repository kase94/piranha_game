﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerRelativeForce : PlayerController
{
    [SerializeField] float _mainThrust = 30f;
    [SerializeField] float _maxVelocity = 100f;
    [SerializeField] float _brakingPower = 0.80f;

    Rigidbody _rigidbody;

    
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }


    public override void Move(float horizontal, float vertical)
    {
        Vector2 movementVector = new Vector2(horizontal, vertical);

        movementVector.Normalize();

        _rigidbody.AddRelativeForce(movementVector * _mainThrust);

        if (_rigidbody.velocity.sqrMagnitude > _maxVelocity)
        {
            _rigidbody.velocity *= _brakingPower;
        }
        
    }
}
