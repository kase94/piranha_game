﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerTranslate : PlayerController
{
    [SerializeField] float _mainThrust = 30f;

    public override void Move(float horizontal, float vertical)
    {
        Vector2 velocity = new Vector2(horizontal, vertical).normalized * _mainThrust;

        transform.Translate(velocity * Time.fixedDeltaTime);
    }
}
