﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerAddForce : PlayerController
{
    [SerializeField] float _mainThrust = 30f;
    [SerializeField] float _maxVelocity = 100f;
    [SerializeField] float _brakingPower = 0.80f;

    Rigidbody _rigidbody;

    
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }


    public override void Move(float horizontal, float vertical)
    {
        Vector2 velocity = new Vector2(horizontal, vertical).normalized * _mainThrust;
        _rigidbody.AddForce(velocity);

        if (_rigidbody.velocity.sqrMagnitude > _maxVelocity)
        {
            _rigidbody.velocity *= _brakingPower;
        }
        
    }
}
