﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    float _vertical, _horizontal;

    [SerializeField] PlayerController _playerController;

    void Update()
    {
        _vertical = Input.GetAxis("Vertical");
        _horizontal = Input.GetAxis("Horizontal");
    }

    private void FixedUpdate()
    {
        _playerController.Move(_horizontal, _vertical);
    }

}
